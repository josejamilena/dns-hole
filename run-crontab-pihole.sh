#!/bin/bash
date > /tmp/run-crontab-pihole.log
cd /home/pi
. ./.profile
cd /home/pi/dns-hole && ./download.sh >>/tmp/run-crontab-pihole.log 2>&1
pihole -g >>/tmp/run-crontab-pihole.log 2>&1
date >> /tmp/run-crontab-pihole.log
